import copy
from typing import Any
from typing import Dict
from typing import List


async def update_network_interfaces(
    hub,
    ctx,
    current_instance: Dict[str, Any],
    new_network_interfaces: List[Dict[str, Any]],
) -> Dict[str, Any]:
    # Update the updatable network interface properties and the access configs
    result = {"result": False, "comment": []}

    existing_network_interfaces = copy.deepcopy(
        current_instance.get("network_interfaces", [])
    )
    for net_intf in new_network_interfaces:
        existing_net_intf = next(
            (
                existing_network_interfaces[idx]
                for idx in range(len(existing_network_interfaces))
                if existing_network_interfaces[idx].get("name") == net_intf.get("name")
            ),
            None,
        )
        if not existing_net_intf:
            break

        # According to documentation, only one access config per instance is supported.

        existing_access_configs = existing_net_intf.get("access_configs", [])
        desired_access_configs = net_intf.get("access_configs", [])

        existing_ac = (
            existing_access_configs[0] if len(existing_access_configs) > 0 else None
        )
        desired_ac = (
            desired_access_configs[0] if len(desired_access_configs) > 0 else None
        )

        if not existing_ac and not desired_ac:
            continue

        elif existing_ac and not desired_ac:
            op_ret = await hub.exec.gcp_api.client.compute.instances.deleteAccessConfig(
                ctx,
                resource_id=current_instance.get("resource_id"),
                access_config=existing_ac.get("name"),
                network_interface=net_intf.get("name"),
            )

            r = await hub.tool.gcp.compute.instances.await_completion(ctx, op_ret)
            if not r["result"]:
                result["comment"] += r["comment"]
                return result

        elif desired_ac and not existing_ac:
            op_ret = await hub.exec.gcp_api.client.compute.instances.addAccessConfig(
                ctx,
                resource_id=current_instance.get("resource_id"),
                network_interface=net_intf.get("name"),
                body=desired_ac,
            )

            r = await hub.tool.gcp.compute.instances.await_completion(ctx, op_ret)
            if not r["result"]:
                result["comment"] += r["comment"]
                return result

        # existing_ac and desired_ac
        elif next(
            (
                key
                for key in desired_ac.keys()
                if desired_ac.get(key) != existing_ac.get(key)
            ),
            None,
        ):
            op_ret = await hub.exec.gcp_api.client.compute.instances.updateAccessConfig(
                ctx,
                resource_id=current_instance.get("resource_id"),
                network_interface=net_intf.get("name"),
                body=desired_ac,
            )

            r = await hub.tool.gcp.compute.instances.await_completion(ctx, op_ret)
            if not r["result"]:
                result["comment"] += r["comment"]
                return result

        # Network interface update
        # Cannot update access config through network interface update function - must be a separate call
        body = hub.tool.gcp.compute.instances.create_dict_body(
            ctx, existing_net_intf, net_intf
        )

        # We want to exclude access configs from update nic operation because it is not supported and
        # it is handled separately.
        body.pop("access_configs", None)
        existing_net_intf.pop("access_configs", None)

        if body != existing_net_intf:
            op_ret = (
                await hub.exec.gcp_api.client.compute.instances.updateNetworkInterface(
                    ctx,
                    resource_id=current_instance.get("resource_id"),
                    network_interface=existing_net_intf.get("name"),
                    body=body,
                )
            )

            ret = await hub.tool.gcp.compute.instances.await_completion(ctx, op_ret)
            if not ret["result"]:
                result["comment"] += ret["comment"]
                return result

    result["result"] = True
    return result


async def await_completion(hub, ctx, api_call_ret: Dict[str, Any]) -> Dict[str, Any]:

    result = {"result": False, "comment": []}

    if not api_call_ret["result"] and not api_call_ret["ret"]:
        result["comment"] += api_call_ret["comment"]
        return result

    if "compute#operation" in api_call_ret["ret"].get("kind"):
        operation = api_call_ret["ret"]

        operation_id = hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
            operation.get("selfLink"), "compute.zone_operations"
        )
        handle_operation_ret = await hub.tool.gcp.operation_utils.handle_operation(
            ctx, operation_id, "compute.instances", True
        )

        if not handle_operation_ret["result"]:
            result["comment"] += handle_operation_ret["comment"]
            return result

    result["result"] = True
    return result


def create_dict_body(
    hub, ctx, current: Dict[str, Any], new: Dict[str, Any]
) -> Dict[str, Any]:
    result = copy.deepcopy(current)
    for key, value in new.items():
        if value is not None and value != current.get(key):
            result[key] = value

    return result
