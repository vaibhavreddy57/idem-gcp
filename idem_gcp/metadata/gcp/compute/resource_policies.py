"""Metadata module for managing Resource Policies."""

PATH = "projects/{project}/regions/{region}/resourcePolicies/{resourcePolicy}"
