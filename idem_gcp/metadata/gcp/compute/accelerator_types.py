"""Metadata module for managing Accelerator Types."""

PATH = "projects/{project}/zones/{zone}/acceleratorTypes/{acceleratorType}"
