"""Metadata module for managing Disk Types."""

PATH = "projects/{project}/zones/{zone}/diskTypes/{diskType}"
