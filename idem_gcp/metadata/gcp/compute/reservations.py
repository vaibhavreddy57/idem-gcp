"""Metadata module for managing Reservations."""

PATH = "projects/{project}/zones/{zone}/reservations/{reservation}"
