"""Metadata module for managing Zone Operations."""

PATH = "projects/{project}/zones/{zone}/operations/{operation}"
