def test_snake(hub):
    data = [("retentionPolicy", "retention_policy")]
    for x in data:
        assert hub.tool.gcp.case.snake(x[0]) == x[1]
        assert hub.tool.gcp.case.camel(x[1], True) == x[0]
