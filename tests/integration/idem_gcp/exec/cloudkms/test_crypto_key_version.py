import pytest


@pytest.mark.asyncio
async def test_crypto_key_version_list(hub, ctx):
    parent = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/cryptoKeys/cicd-key-1"
    ret = await hub.exec.gcp.cloudkms.crypto_key_version.list(ctx, crypto_key=parent)
    assert ret["result"], ret["comment"]
    assert len(ret["ret"]) > 0
    assert ret["ret"][0]["resource_id"] == f"{parent}/cryptoKeyVersions/1"


@pytest.mark.asyncio
async def test_crypto_key_version_list_empty(hub, ctx):
    crypto_key = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/cryptoKeys/cicd-key-1000"
    ret = await hub.exec.gcp.cloudkms.crypto_key_version.list(
        ctx, crypto_key=crypto_key
    )
    assert not ret["result"], ret["comment"]
    assert len(ret["ret"]) == 0
    assert len(ret["comment"]) == 1 and ret["comment"][0].startswith("<HttpError 404 ")


@pytest.mark.asyncio
async def test_crypto_key_version_get(hub, ctx):
    resource_id = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/cryptoKeys/cicd-key-1/cryptoKeyVersions/1"
    ret = await hub.exec.gcp.cloudkms.crypto_key_version.get(
        ctx, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]["resource_id"] == resource_id


@pytest.mark.asyncio
async def test_crypto_key_version_get_empty(hub, ctx):
    resource_id = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/cryptoKeys/cicd-key-1/cryptoKeyVersions/1000"
    ret = await hub.exec.gcp.cloudkms.crypto_key_version.get(
        ctx, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert len(ret["comment"]) == 1 and ret["comment"][
        0
    ] == hub.tool.gcp.comment_utils.get_empty_comment(
        "gcp.cloudkms.crypto_key_version", resource_id
    ), ret[
        "comment"
    ]
