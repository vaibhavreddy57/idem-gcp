import uuid

import pytest
import pytest_asyncio
from pytest_idem import runner

PARAMETER = {
    "instance_name": "idem-test-instance-" + str(uuid.uuid4()),
    "instance3_name": "idem-test-instance-" + str(uuid.uuid4()),
    "disk_name": "idem-test-disk-" + str(uuid.uuid4()),
    "disk2_name": "idem-test-disk-" + str(uuid.uuid4()),
    "disk3_name": "idem-test-disk-" + str(uuid.uuid4()),
}

DISK_SPEC = f"""
{PARAMETER["disk_name"]}:
  gcp.compute.disks.present:
  - zone: us-central1-a
  - project: tango-gcp
  - type: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
  - size_gb: '10'
  - resource_policies:
    - projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1
"""

DISK2_SPEC = f"""
{PARAMETER["disk2_name"]}:
  gcp.compute.disks.present:
  - zone: us-central1-a
  - project: tango-gcp
  - type: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
  - size_gb: '11'
  - resource_policies:
    - projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1
"""

DISK3_SPEC = f"""
{PARAMETER["disk3_name"]}:
  gcp.compute.disks.present:
  - zone: us-central1-a
  - project: tango-gcp
  - type: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
  - size_gb: '10'
  - resource_policies:
    - projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1
"""

ATTACH_DISK_SPEC = """
attach_disk_{device_name}:
  exec.run:
    - path: gcp.compute.instances.attach_disk
    - kwargs:
        resource_id: {resource_id}
        auto_delete: {auto_delete}
        device_name: {device_name}
        source: {source}
        disk_size_gb: {disk_size_gb}
        type: {type}
"""

DETACH_DISK_SPEC = """
attach_disk_{device_name}:
  exec.run:
    - path: gcp.compute.instances.detach_disk
    - kwargs:
        resource_id: {resource_id}
        device_name: {device_name}
"""

INSTANCE_SPEC = """
{name}:
  gcp.compute.instances.present:
  - name: {name}
  - project: tango-gcp
  - machine_type: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small
  - zone: us-central1-a
  - can_ip_forward: false
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        network_tier: PREMIUM
        set_public_ptr: false
        type: ONE_TO_ONE_NAT
      kind: compute#networkInterface
      name: nic0
      network: https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default
      stack_type: IPV4_ONLY
      subnetwork: https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default
  - disks:
    - auto_delete: true
      boot: true
      device_name: {disk}
      source: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/disks/{disk}
      mode: READ_WRITE
      type: PERSISTENT
  - scheduling:
      automatic_restart: true
      on_host_maintenance: MIGRATE
      preemptible: false
      provisioning_model: STANDARD
  - deletion_protection: false
"""

INSTANCE_NO_AC_SPEC = """
{name}:
  gcp.compute.instances.present:
  - name: {name}
  - project: tango-gcp
  - machine_type: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small
  - zone: us-central1-a
  - can_ip_forward: false
  - network_interfaces:
    - access_configs: []
      kind: compute#networkInterface
      name: nic0
      network: https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default
      stack_type: IPV4_ONLY
      subnetwork: https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default
  - disks:
    - auto_delete: true
      boot: true
      device_name: {disk}
      source: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/disks/{disk}
      mode: READ_WRITE
      type: PERSISTENT
  - scheduling:
      automatic_restart: true
      on_host_maintenance: MIGRATE
      preemptible: false
      provisioning_model: STANDARD
  - deletion_protection: false
"""


@pytest_asyncio.fixture(scope="function")
def test_disk(hub, idem_cli):
    global PARAMETER
    if "test_disk" in PARAMETER:
        return PARAMETER["test_disk"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(DISK_SPEC)
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = state_ret[
            f'gcp.compute.disks_|-{PARAMETER["disk_name"]}_|-{PARAMETER["disk_name"]}_|-present'
        ]
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]
        PARAMETER["test_disk"] = new_resource
        return new_resource


@pytest_asyncio.fixture(scope="function")
def test_disk_to_attach(hub, idem_cli):
    global PARAMETER
    if "test_disk_to_attach" in PARAMETER:
        return PARAMETER["test_disk_to_attach"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(DISK2_SPEC)
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = state_ret[
            f'gcp.compute.disks_|-{PARAMETER["disk2_name"]}_|-{PARAMETER["disk2_name"]}_|-present'
        ]
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]
        PARAMETER["test_disk_to_attach"] = new_resource
        return new_resource


@pytest_asyncio.fixture(scope="function")
def test_instance(hub, idem_cli, test_disk):
    global PARAMETER
    if "test_instance" in PARAMETER:
        return PARAMETER["test_instance"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            INSTANCE_SPEC.format(
                **{"name": PARAMETER["instance_name"], "disk": PARAMETER["disk_name"]}
            )
        )
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = state_ret[
            f'gcp.compute.instances_|-{PARAMETER["instance_name"]}_|-{PARAMETER["instance_name"]}_|-present'
        ]
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]
        PARAMETER["resource_id"] = new_resource.get("resource_id", "")
        PARAMETER["test_instance"] = new_resource
        return new_resource


@pytest.mark.asyncio
async def test_instance_list_filter(hub, ctx, test_instance):
    test_instance_name = test_instance["name"]
    ret = await hub.exec.gcp.compute.instances.list(
        ctx,
        zone=test_instance["zone"],
        filter=f"name eq {test_instance_name}",
    )
    assert len(ret["ret"]) == 1
    instance = ret["ret"][0]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance["resource_id"], "compute.instances"
    )
    assert instance["name"] == test_instance_name

    ret = await hub.exec.gcp.compute.instances.list(
        ctx,
        filter=f"name ne {test_instance_name}",
    )
    for instance in ret["ret"]:
        assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
            instance["resource_id"], "compute.instances"
        )
        assert instance["name"] != test_instance_name


@pytest.mark.asyncio
async def test_instance_get_by_resource_id(hub, ctx, test_instance):
    ret = await hub.exec.gcp.compute.instances.get(
        ctx, resource_id=test_instance["resource_id"]
    )
    instance = ret["ret"]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance["resource_id"], "compute.instances"
    )
    assert instance["name"] == test_instance["name"]

    ret = await hub.exec.gcp.compute.instances.get(
        ctx,
        resource_id="@@##$$%%^^",
    )
    assert not ret["ret"]


@pytest.mark.asyncio
async def test_instance_get_by_project_zone_and_name(hub, ctx, test_instance):
    ret = await hub.exec.gcp.compute.instances.get(
        ctx,
        zone=test_instance["zone"],
        instance=test_instance["name"],
    )
    instance = ret["ret"]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance["resource_id"], "compute.instances"
    )
    assert instance["name"] == test_instance["name"]

    ret = await hub.exec.gcp.compute.instances.get(
        ctx,
        instance="@@##$$%%^^",
    )
    assert not ret["ret"]


@pytest.mark.asyncio
async def test_instance_set_disk_auto_delete(hub, ctx, test_instance):
    flag = test_instance["disks"][0]["auto_delete"]
    ret = await hub.exec.gcp.compute.instances.set_disk_auto_delete(
        ctx,
        zone=test_instance["zone"],
        instance=test_instance["name"],
        device_name=test_instance["disks"][0]["device_name"],
        auto_delete=not flag,
    )
    instance = ret["ret"]
    assert instance
    assert (not flag) == instance["disks"][0]["auto_delete"]

    ret = await hub.exec.gcp.compute.instances.set_disk_auto_delete(
        ctx,
        zone=test_instance["zone"],
        instance=test_instance["name"],
        device_name=test_instance["disks"][0]["device_name"],
        auto_delete=flag,
    )
    instance = ret["ret"]
    assert instance
    assert flag == instance["disks"][0]["auto_delete"]


@pytest.mark.asyncio
async def test_instance_attach_disk(
    hub, ctx, idem_cli, test_instance, test_disk_to_attach
):
    # Initially, test instance has only one boot disk
    assert len(test_instance["disks"]) == 1
    assert test_instance["disks"][0]["device_name"] == PARAMETER["disk_name"]

    params = {
        "resource_id": test_instance.get("resource_id"),
        "source": test_disk_to_attach.get("resource_id"),
        "disk_size_gb": test_disk_to_attach.get("size_gb"),
        "type": test_disk_to_attach.get("type"),
        "device_name": test_disk_to_attach.get("name"),
        "auto_delete": False,
    }

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(ATTACH_DISK_SPEC.format(**params))
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = next(
            iter(
                [_ for key, _ in state_ret.items() if f'{params["device_name"]}' in key]
            )
        )

        assert ret["result"], ret["comment"]
        instance = ret["new_state"]

        assert instance
        assert len(instance["disks"]) == 2
        assert instance["disks"][0]["device_name"] == PARAMETER["disk_name"]
        assert instance["disks"][1]["device_name"] == PARAMETER["disk2_name"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(DETACH_DISK_SPEC.format(**params))
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = next(
            iter(
                [_ for key, _ in state_ret.items() if f'{params["device_name"]}' in key]
            )
        )
        assert ret["result"], ret["comment"]
        instance = ret["new_state"]

        assert instance
        assert len(instance["disks"]) == 1
        assert instance["disks"][0]["device_name"] == PARAMETER["disk_name"]


@pytest_asyncio.fixture(scope="function")
def test_disk3(hub, idem_cli):
    global PARAMETER
    if "test_disk3" in PARAMETER:
        return PARAMETER["test_disk3"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(DISK3_SPEC)
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = state_ret[
            f'gcp.compute.disks_|-{PARAMETER["disk3_name"]}_|-{PARAMETER["disk3_name"]}_|-present'
        ]
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]
        PARAMETER["test_disk3"] = new_resource
        return new_resource


@pytest_asyncio.fixture(scope="function")
def test_instance_no_ac(hub, idem_cli, test_disk3):
    global PARAMETER
    if "test_instance3" in PARAMETER:
        return PARAMETER["test_instance3"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            INSTANCE_NO_AC_SPEC.format(
                **{"name": PARAMETER["instance3_name"], "disk": PARAMETER["disk3_name"]}
            )
        )
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = state_ret[
            f'gcp.compute.instances_|-{PARAMETER["instance3_name"]}_|-{PARAMETER["instance3_name"]}_|-present'
        ]
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]
        PARAMETER["resource_id"] = new_resource.get("resource_id", "")
        PARAMETER["test_instance3"] = new_resource
        return new_resource


@pytest.mark.asyncio
async def test_instance_add_remove_access_configs(
    hub, ctx, idem_cli, test_instance_no_ac
):
    assert test_instance_no_ac
    assert len(test_instance_no_ac["network_interfaces"]) == 1
    assert not test_instance_no_ac["network_interfaces"][0].get("access_configs")

    s = INSTANCE_SPEC.format(
        **{"name": PARAMETER["instance3_name"], "disk": PARAMETER["disk3_name"]}
    )
    s += "  - resource_id: " + test_instance_no_ac.get("resource_id")

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(s)
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = state_ret[
            f'gcp.compute.instances_|-{PARAMETER["instance3_name"]}_|-{PARAMETER["instance3_name"]}_|-present'
        ]
        assert ret["result"], ret["comment"]
        instance_with_ac = ret["new_state"]

    assert instance_with_ac
    assert len(instance_with_ac["network_interfaces"]) == 1
    assert len(instance_with_ac["network_interfaces"][0].get("access_configs")) == 1

    # These are the hard coded configs in the INSTANCE_SPEC
    desired_access_config = {
        "kind": "compute#accessConfig",
        "name": "External NAT",
        "network_tier": "PREMIUM",
        "set_public_ptr": False,
        "type": "ONE_TO_ONE_NAT",
    }
    actual_access_config = instance_with_ac["network_interfaces"][0].pop(
        "access_configs"
    )[0]
    for key, value in desired_access_config.items():
        assert value == actual_access_config[key]

    desired_network_interfce_no_a_c = {
        "kind": "compute#networkInterface",
        "name": "nic0",
        "network": "https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default",
        "stack_type": "IPV4_ONLY",
        "subnetwork": "https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default",
    }
    actual_network_interface_no_a_c = instance_with_ac["network_interfaces"][0]
    for key, value in desired_network_interfce_no_a_c.items():
        assert value == actual_network_interface_no_a_c[key]


# TODO: This test should be enhanced once we add functionality for creating firewalls with idem state
#  Until then, we are testing it with the firewalls for the default network.
@pytest.mark.asyncio
async def test_instance_get_effective_firewalls(hub, ctx, idem_cli, test_instance):
    # The network interface of the network with name 'default' which is specified in the test_instance
    network_interface = test_instance["network_interfaces"][0]["name"]

    ret = await hub.exec.gcp.compute.instances.get_effective_firewalls(
        ctx,
        resource_id=test_instance["resource_id"],
        network_interface=network_interface,
    )

    # The 'default' network has 3 firewalls, http, https, ssh. At least one should be returned (ssh),
    # Maybe http and https as well if the traffic is enabled in the specified instance
    assert ret["ret"], ret["result"]
    assert not ret["comment"]
