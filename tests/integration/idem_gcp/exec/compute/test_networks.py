import pytest


@pytest.mark.asyncio
async def test_network_list_filter(hub, ctx):
    ret = await hub.exec.gcp.compute.networks.list(ctx, filter="name eq default")
    assert len(ret["ret"]) == 1
    network = ret["ret"][0]
    assert network["name"] == "default"

    ret = await hub.exec.gcp.compute.networks.list(ctx, filter="name ne default")
    for network in ret["ret"]:
        assert network["name"] != "default"


@pytest.mark.asyncio
async def test_network_get(hub, ctx):
    ret = await hub.exec.gcp.compute.networks.get(ctx, name="default")
    network = ret["ret"]
    assert network["name"] == "default"

    ret = await hub.exec.gcp.compute.networks.get(ctx, name="@@##$$%%^^")
    assert not ret["ret"]
