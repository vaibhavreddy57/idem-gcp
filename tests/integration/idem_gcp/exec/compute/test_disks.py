from typing import Any
from typing import Dict
from typing import List

import pytest


@pytest.fixture(scope="module")
async def gcp_disks(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.disks.list(ctx, zone="us-central1-a")
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_list_project_zone_filter(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]

        ret = await hub.exec.gcp.compute.disks.list(
            ctx,
            zone="us-central1-a",
            filter=f"name eq {disk['name']}",
        )

        assert ret["ret"], ret["result"]
        assert len(ret["ret"]) == 1


@pytest.mark.asyncio
async def test_get_project_zone_name(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]

        ret = await hub.exec.gcp.compute.disks.get(
            ctx,
            zone="us-central1-a",
            name=f"{disk['name']}",
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_disk = ret["ret"]
        assert disk["name"] == returned_disk["name"]
        assert disk.get("resource_id") == returned_disk.get("resource_id")
        assert disk.get("name") in returned_disk.get("name")
        assert disk.get("zone") in returned_disk.get("zone")


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]

        ret = await hub.exec.gcp.compute.disks.get(
            ctx,
            resource_id=disk["resource_id"],
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_disk = ret["ret"]
        assert disk["name"] == returned_disk["name"]
        assert disk.get("resource_id") == returned_disk.get("resource_id")
        assert disk.get("name") in returned_disk.get("name")
        assert disk.get("zone") in returned_disk.get("zone")


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]
        ret = await hub.exec.gcp.compute.disks.get(
            ctx,
            zone=disk.get("zone"),
            name="invalid-name",
        )
        assert ret["result"], not ret["ret"]
        assert "Get compute.disks 'None' result is empty" in ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = "/projects/invalid/zone/invalid/disks/invalid-name"
    ret = await hub.exec.gcp.compute.disks.get(
        ctx,
        resource_id=invalid_resource_id,
    )
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
