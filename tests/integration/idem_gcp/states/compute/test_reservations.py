from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_reservations_describe(hub, ctx):
    ret = await hub.states.gcp.compute.reservations.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.reservations.present")
        assert described_resource
        reservation = dict(ChainMap(*described_resource))
        assert reservation.get("resource_id") == resource_id
