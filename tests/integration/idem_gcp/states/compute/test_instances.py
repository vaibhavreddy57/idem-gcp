import copy
import uuid
from collections import ChainMap

import pytest

from tests.utils import create_external_instance
from tests.utils import delete_instance

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-instance-" + str(uuid.uuid4()),
    "disk_name": "idem-test-disk-" + str(uuid.uuid4()),
    "zone": "us-central1-a",
    "project": "tango-gcp",
    "external_instance_name": "idem-test-ext-instance-" + str(int(uuid.uuid4())),
}

PRESENT_STATE_DISK = {
    "name": PARAMETER["disk_name"],
    "zone": PARAMETER["zone"],
    "project": PARAMETER["project"],
    "type": "https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced",
    "size_gb": "10",
    "physical_block_size_bytes": "4096",
    "resource_policies": [
        "https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1"
    ],
}

PRESENT_CREATE_STATE = {
    "name": PARAMETER["name"],
    "project": PARAMETER["project"],
    "zone": PARAMETER["zone"],
    "machine_type": "https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small",
    "can_ip_forward": False,
    "network_interfaces": [
        {
            "access_configs": [
                {
                    "kind": "compute#accessConfig",
                    "name": "External NAT",
                    "network_tier": "PREMIUM",
                    "set_public_ptr": False,
                    "type": "ONE_TO_ONE_NAT",
                }
            ],
            "kind": "compute#networkInterface",
            "name": "nic0",
            "network": "https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default",
            "stack_type": "IPV4_ONLY",
            "subnetwork": "https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default",
        }
    ],
    "disks": [
        {
            "auto_delete": True,
            "boot": True,
            "device_name": PARAMETER["disk_name"],
            "source": f"https://www.googleapis.com/compute/v1/projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/disks/{PARAMETER['disk_name']}",
            "mode": "READ_WRITE",
            "type": "PERSISTENT",
            "disk_size_gb": "10",
            "index": 0,
            "interface": "SCSI",
            "kind": "compute#attachedDisk",
        }
    ],
    "scheduling": {
        "automatic_restart": True,
        "on_host_maintenance": "MIGRATE",
        "preemptible": False,
        "provisioning_model": "STANDARD",
    },
    "deletion_protection": False,
    "tags": {"items": ["test"]},
    "metadata": {
        "kind": "compute#metadata",
        "items": [{"key": "sample_metadata_key", "value": "sample_metadata_value"}],
    },
}

PRESENT_UPDATED_PROPERTIES = {
    "description": "test description",
    "tags": {"items": ["test1", "test2", "test3"]},
    "can_ip_forward": True,
}

PRESENT_UPDATED_NON_UPDATABLE_PROPERTIES = {
    "disks": [
        {
            "initialize_params": {
                "disk_name": "test_disk_name",
                "source_image": "projects/debian-cloud/global/images/family/debian-11",
                "disk_size_gb": "10",
            }
        }
    ],
    "network_interfaces": [
        {
            "access_configs": [
                {
                    "kind": "compute#accessConfig",
                    "name": "External NAT",
                    "network_tier": "PREMIUM",
                    "set_public_ptr": False,
                    "type": "ONE_TO_ONE_NAT",
                }
            ],
            "kind": "compute#networkInterface",
            "name": "nic0",
            "network": "https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default",
            "stack_type": "IPV4_ONLY",
            "subnetwork": "https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default",
            "aliasIpRanges": [
                {
                    "ipCidrRange": "10.2.3.4",
                }
            ],
        },
        {
            "access_configs": [
                {
                    "kind": "compute#accessConfig",
                    "name": "External NAT",
                    "network_tier": "PREMIUM",
                    "set_public_ptr": False,
                    "type": "ONE_TO_ONE_NAT",
                }
            ],
            "network": "https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default",
            "stack_type": "IPV4_ONLY",
            "subnetwork": "https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default",
            "network_ip": "10.128.0.35",
        },
    ],
}

NON_UPDATABLE_PROPERTIES_PATHS = {"disks[].initialize_params"}

RESOURCE_ID = f"projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/instances/{PARAMETER['name']}"

RESOURCE_TYPE_INSTANCES = "compute.instances"
RESOURCE_TYPE_DISKS = "compute.disks"

EXTERNAL_INSTANCE_SPEC = f"""
{PARAMETER["external_instance_name"]}:
  gcp.compute.instances.present:
  - name: {PARAMETER["external_instance_name"]}
  - project: tango-gcp
  - machine_type: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small
  - zone: us-central1-a
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        type: ONE_TO_ONE_NAT
      kind: compute#networkInterface
      network: https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default
  - disks:
    - auto_delete: true
      boot: true
"""


@pytest.fixture(scope="function")
def new_disk(hub, idem_cli):
    disk_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_DISKS, PRESENT_STATE_DISK
    )
    assert disk_present_ret["result"], disk_present_ret["comment"]

    resource_id = disk_present_ret["new_state"]["resource_id"]
    expected_state = {"resource_id": resource_id, **PRESENT_STATE_DISK}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(disk_present_ret["new_state"]), RESOURCE_TYPE_DISKS
    )
    assert not bool(changes), changes

    yield disk_present_ret

    disk_absent_ret = hub.tool.utils.call_absent(
        idem_cli, "compute.disks", disk_present_ret["new_state"]["name"], resource_id
    )
    assert disk_absent_ret["result"], disk_absent_ret["comment"]
    assert not disk_absent_ret.get("new_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_instance_present_create(hub, idem_cli, tests_dir, __test, new_disk):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCES, PRESENT_CREATE_STATE, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    expected_state = {"resource_id": RESOURCE_ID, **PRESENT_CREATE_STATE}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )
    assert not bool(changes), changes


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_update", depends=["present_create"])
def test_instance_present_update(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCES, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    expected_state = {"resource_id": RESOURCE_ID, **update_present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )
    assert not bool(changes), changes


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_already_exists", depends=["present_update"])
def test_instance_present_already_exists(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCES, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.already_exists_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
        )
    ] == ret["comment"]

    expected_state = {"resource_id": RESOURCE_ID, **update_present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_changed_non_updatable_properties", depends=["present_already_exists"]
)
def test_instance_present_changed_non_updatable_properties(
    hub, idem_cli, tests_dir, __test
):
    global PARAMETER
    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_NON_UPDATABLE_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCES, update_present_state, __test
    )

    assert not ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.non_updatable_properties_comment(
            f"gcp.{RESOURCE_TYPE_INSTANCES}",
            PARAMETER["name"],
            NON_UPDATABLE_PROPERTIES_PATHS,
        )
    ] == ret["comment"]

    # This is the current old_state and new_state after the failed update above
    present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    present_state.update(PRESENT_UPDATED_PROPERTIES)

    expected_state = {"resource_id": RESOURCE_ID, **present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="describe", depends=["present_changed_non_updatable_properties"]
)
async def test_instance_describe(hub, ctx):
    describe_ret = await hub.states.gcp.compute.instances.describe(ctx)
    assert RESOURCE_ID in describe_ret
    assert f"gcp.{RESOURCE_TYPE_INSTANCES}.present" in describe_ret[RESOURCE_ID]
    described_resource = describe_ret[RESOURCE_ID].get(
        f"gcp.{RESOURCE_TYPE_INSTANCES}.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["name"] == described_resource_map["name"]
    assert RESOURCE_ID == described_resource_map["resource_id"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
def test_instance_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE_INSTANCES, PARAMETER["name"], RESOURCE_ID, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_PROPERTIES)
    expected_state = {"resource_id": RESOURCE_ID, **update_present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["old_state"]), RESOURCE_TYPE_INSTANCES
    )

    assert not bool(changes), changes
    assert not ret.get("new_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
def test_instance_already_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE_INSTANCES, PARAMETER["name"], RESOURCE_ID, __test
    )

    assert [
        hub.tool.gcp.comment_utils.already_absent_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
        )
    ] == ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")


@pytest.mark.asyncio
async def test_discover_external_instance(hub, ctx, idem_cli, tests_dir):
    project = "tango-gcp"
    zone = "us-central1-a"
    name = PARAMETER["external_instance_name"]

    # creates a GCP instance using the GCP API client library without going through idem
    create_external_instance(ctx, project, zone, name)

    ret = hub.tool.utils.call_present_from_sls(idem_cli, EXTERNAL_INSTANCE_SPEC)

    assert ret["result"]
    assert (
        hub.tool.gcp.comment_utils.already_exists_comment("gcp.compute.instances", name)
        in ret["comment"]
    )
    assert ret["new_state"] == ret["old_state"]
    assert (
        ret["new_state"]["resource_id"]
        == f"projects/{project}/zones/{zone}/instances/{name}"
    )

    # deletes the created instance using the GCP API client library
    delete_instance(ctx, project, zone, name)
