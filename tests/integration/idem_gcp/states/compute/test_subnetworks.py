from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_present_no_op(hub, ctx):
    ret = await hub.states.gcp.compute.subnetworks.present(
        ctx, "name", "project", "region"
    )
    assert ret
    assert ret["result"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert not ret["rerun_data"]
    assert ret["comment"] == [
        "No-op: There is no create/update function for gcp.compute.subnetworks"
    ]


@pytest.mark.asyncio
async def test_absent_no_op(hub, ctx):
    ret = await hub.states.gcp.compute.subnetworks.absent(
        ctx, "name", "project", "region"
    )
    assert ret
    assert ret["result"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert not ret["rerun_data"]
    assert ret["comment"] == [
        "No-op: There is no absent function for gcp.compute.subnetworks"
    ]


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.compute.subnetworks.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.subnetworks.present")
        assert described_resource
        subnetwork = dict(ChainMap(*described_resource))
        assert subnetwork.get("resource_id") == resource_id
