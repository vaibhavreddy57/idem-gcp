import uuid
from collections import ChainMap

import pytest
import pytest_asyncio
from pytest_idem import runner

PARAMETER = {
    "network_name": "idem-test-network-" + str(uuid.uuid4()),
    "network2_name": "idem-test-network-" + str(uuid.uuid4()),
    "peering_name": "idem-test-peering-" + str(uuid.uuid4()),
}

NETWORK_SPEC = """
{network_name}:
  gcp.compute.networks.present:
  - project: tango-gcp
  - name: {network_name}
  - description: ''
  - auto_create_subnetworks: true
  - peerings: []
  - routing_config:
      routing_mode: REGIONAL
  - mtu: 1460
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
"""

NETWORK_WITH_PEERING_SPEC = """
{network_name}:
  gcp.compute.networks.present:
  - project: tango-gcp
  - name: {network_name}
  - description: ''
  - auto_create_subnetworks: true
  - peerings:
    - auto_create_routes: true
      exchange_subnet_routes: true
      export_custom_routes: false
      export_subnet_routes_with_public_ip: true
      import_custom_routes: false
      import_subnet_routes_with_public_ip: false
      name: {peering_name}
      network: {peering_network}
      peer_mtu: 1520
  - routing_config:
      routing_mode: REGIONAL
  - mtu: 1460
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
"""

ABSENT_SPEC = """
test_network_absent_1:
  gcp.compute.networks.absent:
  - resource_id: {resource_id}
"""


@pytest_asyncio.fixture(scope="function")
def test_networks(hub, ctx, idem_cli):
    global PARAMETER
    if "test_network" in PARAMETER and "test_network2" in PARAMETER:
        return [PARAMETER["test_network"], PARAMETER["test_network2"]]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_SPEC.format(**{"network_name": PARAMETER["network_name"]})
        )
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.networks_|" in key])
        )
        assert ret["result"], ret["comment"]
        network = ret["new_state"]
        PARAMETER["test_network"] = network

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_WITH_PEERING_SPEC.format(
                **{
                    "network_name": PARAMETER["network2_name"],
                    "peering_name": PARAMETER["peering_name"],
                    "peering_network": network.get("resource_id"),
                }
            )
        )
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.networks_|" in key])
        )
        assert ret["result"], ret["comment"]
        network = ret["new_state"]
        PARAMETER["test_network2"] = network

    return [PARAMETER["test_network"], PARAMETER["test_network2"]]


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_update_remove_peering")
async def test_network_remove_peering(hub, ctx, idem_cli, test_networks):
    assert len(test_networks) == 2

    assert test_networks[0].get("name") == PARAMETER["network_name"]
    assert not test_networks[0].get("peerings")

    assert test_networks[1].get("name") == PARAMETER["network2_name"]
    assert len(test_networks[1].get("peerings")) == 1

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_SPEC.format(
                **{
                    "network_name": PARAMETER["network2_name"],
                }
            )
            + "  - resource_id: "
            + PARAMETER["test_network2"].get("resource_id")
        )
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.networks_|" in key])
        )
        assert ret["result"], ret["comment"]
        network = ret["new_state"]

        assert network.get("name") == PARAMETER["network2_name"]
        assert len(network.get("peerings", [])) == 0

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_WITH_PEERING_SPEC.format(
                **{
                    "network_name": PARAMETER["network2_name"],
                    "peering_name": PARAMETER["peering_name"],
                    "peering_network": PARAMETER["test_network"].get("resource_id"),
                }
            )
            + "  - resource_id: "
            + PARAMETER["test_network2"].get("resource_id")
        )
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.networks_|" in key])
        )
        assert ret["result"], ret["comment"]
        network = ret["new_state"]

        assert network.get("name") == PARAMETER["network2_name"]
        assert len(network.get("peerings", [])) == 1


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["present_update_remove_peering"])
async def test_cleanup(hub, ctx, idem_cli, test_networks):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            ABSENT_SPEC.format(
                **{"resource_id": PARAMETER["test_network2"].get("resource_id")}
            )
        )
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.networks_|" in key])
        )
        assert ret["result"], ret["comment"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            ABSENT_SPEC.format(
                **{"resource_id": PARAMETER["test_network"].get("resource_id")}
            )
        )
        state_ret = idem_cli("state", fh, "--reconcile=gcp", check=True).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.networks_|" in key])
        )
        assert ret["result"], ret["comment"]

    ret = await hub.exec.gcp.compute.networks.get(
        ctx, resource_id=PARAMETER["test_network"].get("resource_id")
    )
    assert ret["result"]
    assert not ret["ret"]

    ret = await hub.exec.gcp.compute.networks.get(
        ctx, resource_id=PARAMETER["test_network2"].get("resource_id")
    )
    assert ret["result"]
    assert not ret["ret"]


@pytest.mark.asyncio
async def test_network_describe(hub, ctx):
    ret = await hub.states.gcp.compute.networks.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.networks.present")
        assert described_resource
        network = dict(ChainMap(*described_resource))
        assert network.get("resource_id") == resource_id
