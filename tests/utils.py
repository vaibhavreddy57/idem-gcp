"""Creates and deletes GCP instance using the

Usage pattern taken from: https://github.com/GoogleCloudPlatform/python-docs-samples/blob/81a8413528860ff368a3e9f3b7f4e3665588d07f/compute/api/create_instance.py

Running the methods requires the GOOGLE_APPLICATION_CREDENTIALS environment variable
to contain the file path to a service account credentials JSON file
- as obtained by the GCP console
"""
import googleapiclient


def create_external_instance(ctx, project: str, zone: str, name: str):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        source_disk_image = (
            "projects/debian-cloud/global/images/debian-11-bullseye-v20221102"
        )
        config = {
            "name": name,
            "machineType": "https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small",
            "disks": [
                {
                    "boot": True,
                    "autoDelete": True,
                    "initializeParams": {
                        "sourceImage": source_disk_image,
                    },
                }
            ],
            "networkInterfaces": [
                {
                    "network": "https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default",
                    "accessConfigs": [
                        {"type": "ONE_TO_ONE_NAT", "name": "External NAT"}
                    ],
                }
            ],
        }
        return (
            compute_service.instances()
            .insert(project=project, zone=zone, body=config)
            .execute()
        )


def delete_instance(ctx, project: str, zone: str, name: str):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        compute_service.instances().delete(
            project=project, zone=zone, instance=name
        ).execute()
